﻿using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;

namespace TodoTutorial.Models
{
    [Table(Name="Todos")]
    public class Todo
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column]
        public string Title { get; set; }
        [Column]
        public bool Completed { get; set; }
        [Column]
        public DateTime DateAdded { get; set; }
        
        public static Todo[] GetAll()
        {
            DataContext context = new DataContext("Data Source=localhost;Database=TodoTutorial;Integrated Security=True");
            Table<Todo> todos = context.GetTable<Todo>();

            return (from todo in todos
                    select todo).ToArray();
        }
    }
}